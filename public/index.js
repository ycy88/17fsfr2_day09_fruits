(function(){
  angular
    .module("ShoppingApp", [ "CartSvc" ] )
    .controller("ShopC", ShopC)

ShopC.$inject = [ "CartSvc" ];

//controller for hoisting
function ShopC(cartS) {
  // instantiate controller 
  var shopC = this;

  shopC.inventory = cartS.inventory;

  // initialize with null
  shopC.selection = "0";
  shopC.getSelected = function(){
    return (shopC.inventory[+shopC.selection]);
  };

  // cart functions
  shopC.cart = [];
  shopC.qty = 0;

  // addToCart takes 2 inputs - 
  shopC.addToCart = function(){
    shopC.cart = cartS.addToCart(+shopC.selection, +shopC.qty); //returns updated cart
    // clear stuff
    shopC.selection = "0";
    shopC.qty = 0;
  }; // end addToCart

}// close controller 
  


})();



