const express = require('express');

const app = express();

if (process.env.PROD) {
  console.log("I am running in production");
} else {
  console.log("This is testing");
}

// routes
app.use(express.static(__dirname + "/public"));
app.use("/libs", express.static(__dirname + "/bower_components"));

var port = 3000;

app.listen(port, function(){
  console.log("Application started on port on %d", port);
});